<?php
/**
 *  Gravatar Mirror
 *
 *  @author CRH380A-2722 <zs6096@gmail.com>
 */

class Gravatar {

  /**
   *  Gravatar源地址存储对象
   *
   *  @var string
   */

  private $_originUrl = "https://secure.gravatar.com/avatar";

  /**
   *  cURL句柄存储对象
   *
   *  @var resource
   */

  private $_curlHandle;

  /**
   *  浏览器标识存储对象
   *
   *  @var string
   *  @note 这里使用我个人的浏览器标识
   */

  private $_userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36";

  /**
   *  头像文件名存储对象
   *
   *  @var string
   */

  private $_fileName = "";

  /**
   *  构造函数
   *
   *  @return void
   */

  public function __construct() {
    $this->_curlHandle = curl_init();
    curl_setopt($this->_curlHandle, CURLOPT_HEADER, false);
    curl_setopt($this->_curlHandle, CURLOPT_RETURNTRANSFER, false);
    curl_setopt($this->_curlHandle, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($this->_curlHandle, CURLOPT_USERAGENT, $this->_userAgent);
  }

  /**
   *  直接通过PATH_INFO和QUERY_STRING获取参数构建URL (MirrorMode)
   *
   *  @return Gravatar
   */

  public function mirror() {
    $email = $_SERVER['PATH_INFO'];
    $params = $_SERVER['QUERY_STRING'];
    $this->_fileName = str_ireplace("/", "", $email);
    $url = $this->_originUrl . $email;
    if (!empty($params)) $url .= "?" . $params;
    curl_setopt($this->_curlHandle, CURLOPT_URL, $url);
    return $this;
  }

  /**
   *  通过传参构建URL (NormalMode)
   *
   *  @param string $email 用户邮箱地址
   *  @param int $size 头像大小
   *  @param string $level 头像最高可见等级 (G, PG, R, X)
   *  @param string $default 默认头像
   *  @return Gravatar
   */

  public function param($email, $size = 80, $level = "G", $default = "") {
    if (!is_numeric($size)) $this->_error("You must input a correct avatar size. (int)");
    if (!empty($email)) {
      $email = md5(strtolower(trim($email)));
      $this->_fileName = $email;
      $url = $this->_originUrl . "/" . $email . "?s=" . $size . "&r=" . $level . "&d=" . $default;
    } else {
      $url = $this->_originUrl . "?s=" . $size . "&r=" . $level . "&d=" . $default;
    }
    curl_setopt($this->_curlHandle, CURLOPT_URL, $url);
    return $this;
  }

  /**
   *  显示头像
   *
   *  @return void
   */

  public function show() {
    $fileName = (empty($this->_fileName)) ? 'avatar' : $this->_fileName;
    header("Content-Type: image/jpg\n");
    header("Content-Disposition: inline; filename=\"{$fileName}.jpg\"\n");
    curl_exec($this->_curlHandle);
  }

  /**
   *  显示错误并中止脚本
   *
   *  @param string $message 错误信息
   *  @return void
   */

  private function _error($message = "") {
    print "ERROR: " . $message;
    die();
  }

  /**
   *  脚本执行结束前的指令
   *
   *  @return void
   */

  public function __destruct() {
    curl_close($this->_curlHandle);
  }

}
