<?php
/**
 *  Gravatar Mirror
 *
 *  @author CRH380A-2722 <zs6096@gmail.com>
 */

//开启Output Bufferring
ob_start();

//定义根目录
define("BASE_ROOT", dirname(__FILE__));

//引用Gravatar类
require BASE_ROOT . DIRECTORY_SEPARATOR . 'class' . DIRECTORY_SEPARATOR . 'Gravatar.php';

//启动镜像
$avatar = new Gravatar();
$avatar->mirror()->show();

//冲掉Output BUfferring
ob_end_flush();
